function addMarker(data, photo) {

    if(photo) {
        console.log(photo);

        var contentString = '<div id="infoWindow">' + 
            '<a href="' + photo.link + '" target="_blank">' + 
            '<img src="' + photo.images.thumbnail.url + '" />' +
            '</a><br>' +
            photo.location.name +
            '</div>';

        var marker = $map.addMarker({
            lat: data.lat,
            lng: data.lng,
            title: data.title,
            infoWindow: {
                content: contentString
            }
        });
    } else {
        var marker = $map.addMarker({
            lat: data.lat,
            lng: data.lng,
            title: data.title
        });
    }    
}

function createPhotoElement(photo) {
    var innerHtml = $('<img>').addClass('instagram-image').attr('src', photo.images.thumbnail.url);
    innerHtml = $('<a>').attr('target', '_blank').attr('href', photo.link).append(innerHtml);
    return $('<div>').addClass('instagram-placeholder').attr('id', photo.id).append(innerHtml);
}

function didLoadInstagram(event, response) {
    var that = this;
    var name = false;
    var lat, lng;

    $(that).empty();
    $map.removeMarkers();

    //console.log(response.data);

    if(response.data.length != 0) {
        var oldI = -1;

        $.each(response.data, function(i, photo) {
            $(that).append(createPhotoElement(photo));      

            var data = { lat: photo.location.latitude, lng: photo.location.longitude, title: "" };
            addMarker(data, photo);

            if(!name && photo.location.name.length > 0) {
                name = photo.location.name;
                lat = photo.location.latitude;
                lng = photo.location.longitude;
            }
        });

        $.ajax({
            url:'/scripts/setLocation.php?lat=' + lat + '&lng=' + lng + '&name=' + name,
            complete: function (response) {
               // $data = response.responseText;
               // $('.instagram').html( $data );
            }
        });


        if($map.getZoom() > 10) {
            $map.fitZoom();
        }

        $instagram.waitForImages().done(function() {
            //console.log( $('#footer').outerHeight() );

            if($instagram.height() > 200) {
                var footerHeight = $('#footer').outerHeight();
                $instagram.css('padding-bottom', footerHeight + 110);
            }
        });
    } else {
        $(that).append("Nothing here, try to search somewhere else...");
        $instagram.css('padding-bottom', 10);
    }
}

function showPhotos(lat, lng) {
    $instagram.one('didLoadInstagram', didLoadInstagram);
    $instagram.instagram({
        search: {
            lat: lat,
            lng: lng,
            distance: 5000
        },
        count: 30,
        clientId: $clientId
    });
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

/*
================================= DOCUMENT READY =================================
*/
$(document).ready(function() {
    $('#favouritesBox').hide();

    $clientId = '77fe1ee4c776475f8b8e6b4157fa4438';
    $instagram = $('.instagram');
    $infoWindow = null;
    $boxOn = false;
    $data = null;
    
    $lat = getUrlParameter('lat');
    $lng = getUrlParameter('lng');

    if($lat != null && $lng != null) {
        $zoom = 14;
        showPhotos($lat, $lng);
    } else {
        $zoom = 6;
        $lat = 50.0878114;
        $lng = 14.4204598;
    }

    $map = new GMaps({
        div: '#map',
        lat: $lat,
        lng: $lng,
        zoom: $zoom,
        click: function(e) {
            var lat = e.latLng.lat();
            var lng = e.latLng.lng();

            showPhotos(lat, lng);
        }
    });

    $("#showFavourites").click(function() {
        if(!$boxOn) {
            $boxOn = true;
            
            if($data == null) {
                $('#loadingImage').show();
                $.ajax({
                    url:'/scripts/getTopTen.php',
                    complete: function (response) {
                        $('#loadingImage').hide();
                        $data = response.responseText;
                        $('#favouritesBox').html( $data );
                        $('#favouritesBox').show();
                    }
                });
            } else {
                $('#favouritesBox').show();
            }
        } else {
            $boxOn = false;
            $('#favouritesBox').hide();        
        }
    });

    $box = $("#aboutBox");
    var marginL = ($(window).width() - $box.width()) / 2;
    var marginT = ($(window).height() - $box.height()) / 2;
    $box.css({
        'margin-left' : marginL,
        'margin-top' : marginT
    });

    $("#about").click(function() {
        $box.toggle();
    });

    $box.click(function() { $box.hide(); });

});