<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://instamap.apispark.net/v1/locations/?\$sort=occ+DESC&\$page=1&\$size=10",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "authorization: Basic MmNmOWMxOTAtMTE2My00YmQ4LTlhOWUtM2NlZWQ0MzUwMDEwOjdmNmU1MTgwLTRlYzEtNDA1Ni05Zjg1LTA5MzE0MGI3MjZlOQ==",
        "content-type: application/json",
        "host: instamap.apispark.net"
    ),
));

$data = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if (!$err) {
    $data = json_decode($data);
    $response = "";

    foreach ($data as $key => $value) {
        $inner = (array) $value;

        $name = $inner['name'];
        if($name==null) {
            $name = $inner['lat'] . ", " . $inner['lng'];
        }

        echo "<a href='?lat=" . $inner['lat'] . "&lng=" . $inner['lng'] . "'>" . $name . "</a><br>";
    }

    echo $response;
}

?>