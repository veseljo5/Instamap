<?php

$data = $_GET;

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://instamap.apispark.net/v1/locations/?name=" . str_replace(' ', '+', $data['name']),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "authorization: Basic MmNmOWMxOTAtMTE2My00YmQ4LTlhOWUtM2NlZWQ0MzUwMDEwOjdmNmU1MTgwLTRlYzEtNDA1Ni05Zjg1LTA5MzE0MGI3MjZlOQ==",
        "content-type: application/json",
        "host: instamap.apispark.net"
    ),
));
$result = json_decode( curl_exec($curl) );
curl_close($curl);

if( !empty($result) ) {
	$result = (array) $result[0];
	$result['occ']++;

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://instamap.apispark.net/v1/locations/" . $result['id'],
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "PUT",
	  CURLOPT_POSTFIELDS => json_encode($result),
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "authorization: Basic MmNmOWMxOTAtMTE2My00YmQ4LTlhOWUtM2NlZWQ0MzUwMDEwOjdmNmU1MTgwLTRlYzEtNDA1Ni05Zjg1LTA5MzE0MGI3MjZlOQ==",
	    "content-type: application/json",
	    "host: instamap.apispark.net"
	  ),
	));
	curl_exec($curl);
	curl_close($curl);
} else {
	$data['occ'] = 1;

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt_array($curl, array(
	    CURLOPT_URL => "https://instamap.apispark.net/v1/locations/",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_POSTFIELDS => json_encode($data),
	    CURLOPT_HTTPHEADER => array(
	      "accept: application/json",
	      "authorization: Basic MmNmOWMxOTAtMTE2My00YmQ4LTlhOWUtM2NlZWQ0MzUwMDEwOjdmNmU1MTgwLTRlYzEtNDA1Ni05Zjg1LTA5MzE0MGI3MjZlOQ==",
	      "content-type: application/json",
	      "host: instamap.apispark.net"
	      ),
    ));

	curl_exec($curl);
	curl_close($curl);
}

?>